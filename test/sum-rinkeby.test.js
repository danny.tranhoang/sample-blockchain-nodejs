const assert = require('assert')
const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')
const contract = require('../compile')
const config = require('../config')

const provider = new HDWalletProvider(
    config.SECRET_WORDS, config.API_GATEWAY
)

const web3 = new Web3(provider)

let accounts
let myContract
const INIT_NAME = "Danny"

beforeEach(async () => {
    accounts = await web3.eth.getAccounts()

    myContract = await new web3.eth.Contract(JSON.parse(contract.interface))
        .deploy({data: contract.bytecode, arguments: [INIT_NAME]})
        .send({from: accounts[0], gas: 1000000})
})

describe('Sum on Rinkeby is ok?', () => {
    it('Sum function work correctly', async () => {
        await myContract.methods.setNum1(1).send({from: accounts[0]})
        await myContract.methods.setNum2(1).send({from: accounts[0]})
        await myContract.methods.sum().send({gas: 1000000, from: accounts[0]})
        const sum = await myContract.methods.num3().call()
        assert.equal(sum, 2)
    })
})