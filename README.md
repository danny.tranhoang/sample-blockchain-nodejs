# SAMPLE BLOCKCHAIN NODEJS

## Requisite
nodejs : v10.1.0

npm : 5.6.0

## Installation

clone repository
> git clone  https://gitlab.com/danny.tranhoang/sample-blockchain-nodejs.git

come to project root
> cd sample-clockchain-nodejs

install require packages

> npm i

start local server
> npm start

open browser at http://localhost:3000
