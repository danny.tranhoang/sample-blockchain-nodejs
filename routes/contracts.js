var express = require('express');
var router = express.Router();

let {index, deploy, transfer} = require('../controllers/ECR20_ContractController')

/* GET users listing. */
router.get('/', index);
router.post('/deploy', deploy)
router.post('/transfer', transfer)

module.exports = router;
