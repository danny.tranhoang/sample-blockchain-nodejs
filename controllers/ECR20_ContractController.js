const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')
const contract = require('../bin/compile')
const config = require('../configs/account')
const fs = require('fs')
const path = require('path')
const contractFilePath = path.join(__dirname, '../contract_address.txt');

const provider = new HDWalletProvider(
    config.SECRET_WORDS, config.API_GATEWAY, 0, 5
)

const web3 = new Web3(provider)

let accounts
let ECR20Contract

const index = async (req, res, next) => {
    accounts = await web3.eth.getAccounts()
    console.log(accounts)
    res.render('contracts-index', {accounts})
}

const createNewContract = async (_ECR20Contract, address) => {
    const result = await _ECR20Contract.deploy({data: contract.bytecode}).send({
        gas: 1000000,
        from: address
    })

    fs.writeFile(contractFilePath, result.options.address, function (err) {
        if (err) {
            resolve(false)
        } else {
            ECR20Contract = _ECR20Contract
            resolve(true)
        }
    })
}


const deploy = async (req, res, next) => {
    const address = req.body.address

    console.log(req.body.address)

    try {
        fs.readFile(contractFilePath, {encoding: 'utf-8'}, function (err, contract_address) {
            if (!err) {
                console.log("Contract address: ", contract_address)
                if (!contract_address) {
                    createNewContract(new web3.eth.Contract(JSON.parse(contract.interface)), address).then((resp) => {
                        console.log("After write: ", resp)
                        if (resp) {
                            res.send("Save contract OK!")
                        } else {
                            return res.status(400).send({
                                message: "Save contract address error!"
                            })
                        }
                    })
                } else {
                    console.log(contract_address)
                    ECR20Contract = new web3.eth.Contract(JSON.parse(contract.interface), contract_address)
                    res.send("Retrieve existing contract OK!")
                }
            } else {
                return res.status(400).send({
                    message: "Retrieve contract address error!"
                })
            }
        });
    } catch (e) {
        console.log(e)
        return res.status(400).send({
            message: e
        });
    }

}

const transfer = async (req, res, next) => {
    try {
        await ECR20Contract.methods.transfer('0x85B581604b7930BB33b6DcB63c8b46D263352a76', 123123123).send({
            gas: 1000000,
            from: accounts[0]
        })
        res.send("OK")
    } catch (e) {
        console.log(e);
        res.status(400).send({
            message: "Transfer error!"
        })
    }
}

// TODO
// web3.getaolcontract(address)
// get deployed contract by address

// const deploy = async (gas) => {
//     try {
//         const accounts = await web3.eth.getAccounts()
//
//         console.log('Get first account: ', accounts[0])
//         console.log('Get accounts: ', accounts)
//
//         const result = await new web3.eth.Contract(JSON.parse(contract.interface))
//             .deploy({data: contract.bytecode, arguments: ["Hello Danny!"]})
//             .send({gas: gas, from: accounts[0]})
//
//         console.log('Contract is deployed to: ', result.options.address)
//         return result
//     } catch (err) {
//         console.log(err)
//     }
// }
//
// const updateContractName = async (name) => {
//     try {
//         const result = await deploy(1000000)
//         await getAccounts()
//         const finish = await result.methods.setName(name).send({gas: 1000000, from: accounts[0]})
//         console.log("Contract's name has been updated: ", name)
//         console.log("New contract: ", finish)
//     } catch (err) {
//         console.log("Set name error: ", err)
//     }
// }
//
// const upadteContractGas = (gas) => {
//     try {
//         deploy(gas)
//         contract.methods.setGas(gas)
//         console.log("Contract's gas has been updated: ", gas)
//         console.log("New contract: ", contract)
//     } catch (err) {
//         console.log("Set gas error: ", err)
//     }
// }

module.exports = {
    index,
    deploy,
    transfer
};
